package game_of_life

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

type AlwaysFailingGridManagerStub struct {
}

func (a AlwaysFailingGridManagerStub) exists(i int) bool {
	//TODO implement me
	panic("implement me")
}

func (a AlwaysFailingGridManagerStub) GetLastGrid() Grid {
	//TODO implement me
	panic("implement me")
}

func (a AlwaysFailingGridManagerStub) AddGrid(g Grid) {
	//TODO implement me
	panic("implement me")
}

func (a AlwaysFailingGridManagerStub) HasInitialGrid() bool {
	//TODO implement me
	panic("implement me")
}

func TestGameOfLifeRunner(t *testing.T) {
	t.Run("run of one generation", func(t *testing.T) {
		gridManager := NewGridManagerStub()
		runner := GameOfLifeRunner{gridManager}

		err := runner.ComputeLastGeneration()
		assert.NoError(t, err)

		gridExists := runner.GridManager.exists(1)
		assert.True(t, gridExists)
	})

	t.Run("no grid", func(t *testing.T) {
		gridManager := NewGridManagerStub()
		runner := GameOfLifeRunner{gridManager}

		gridManager.Clear()

		err := runner.ComputeLastGeneration()
		assert.Equal(t, err, errors.New("no initial grid"))

	})

	t.Run("can't load grid", func(t *testing.T) {
		gridManager := AlwaysFailingGridManagerStub{}
		runner := GameOfLifeRunner{gridManager}

		err := runner.ComputeLastGeneration()

		assert.Equal(t, err, errors.New("can't load last grid"))
	})
}
