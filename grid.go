package game_of_life

import "math"

type Grid struct {
	sideLength int
	cells      [][]bool
}

func NewEmptyGrid(len int) Grid {
	cells := make([][]bool, len)
	for i := range cells {
		cells[i] = make([]bool, len)
	}

	return Grid{
		sideLength: len,
		cells:      cells,
	}
}

func NewGridFromCells(cells [][]bool) (grid Grid) {
	return Grid{
		sideLength: len(cells),
		cells:      cells,
	}
}

func (grid Grid) countAliveNeighbours(i int, j int) (numberOfAliveNeighbours int) {
	neighborsIndexes := grid.getNeighborsSquareIndexes(i, j)

	for x := neighborsIndexes.xMin; x <= neighborsIndexes.xMax; x++ {
		for y := neighborsIndexes.yMin; y <= neighborsIndexes.yMax; y++ {
			if x == i && y == j || !grid.cells[x][y] {
				continue
			}
			numberOfAliveNeighbours++
		}
	}
	return numberOfAliveNeighbours
}

type SquareIndexes struct {
	xMin int
	xMax int
	yMin int
	yMax int
}

func (grid Grid) getNeighborsSquareIndexes(i int, j int) SquareIndexes {
	return SquareIndexes{
		xMin: int(math.Max(float64(i-1), 0)),
		xMax: int(math.Min(float64(i+1), float64(grid.sideLength-1))),
		yMin: int(math.Max(float64(j-1), 0)),
		yMax: int(math.Min(float64(j+1), float64(grid.sideLength-1))),
	}
}
