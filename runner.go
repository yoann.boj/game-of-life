package game_of_life

import "errors"

type GridManager interface {
	exists(i int) bool
	GetLastGrid() Grid
	AddGrid(g Grid)
	HasInitialGrid() bool
}

type GridManagerStub struct {
	grids []Grid
}

func (g GridManagerStub) HasInitialGrid() bool {
	return g.exists(0)
}

func (g GridManagerStub) GetLastGrid() Grid {
	return g.grids[len(g.grids)-1]
}

func (g *GridManagerStub) AddGrid(grid Grid) {
	g.grids = append(g.grids, grid)
}

func NewGridManagerStub() *GridManagerStub {
	g := GridManagerStub{}
	g.grids = append(g.grids, Grid{})
	return &g
}

func (g GridManagerStub) exists(i int) bool {
	return len(g.grids) > i
}

func (g *GridManagerStub) Clear() {
	g.grids = make([]Grid, 0)
}

type GameOfLifeRunner struct {
	GridManager GridManager
}

func (r GameOfLifeRunner) ComputeLastGeneration() error {
	if !r.GridManager.HasInitialGrid() {
		return errors.New("no initial grid")
	}

	newGrid := ComputeNextGeneration(r.GridManager.GetLastGrid())
	r.GridManager.AddGrid(newGrid)

	return nil
}
