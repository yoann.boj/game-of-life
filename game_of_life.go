package game_of_life

func ComputeNextGeneration(grid Grid) (nextGenGrid Grid) {
	nextGenGrid = NewEmptyGrid(grid.sideLength)

	for i := range grid.cells {
		for j, cellState := range grid.cells[i] {
			aliveNeighboursCount := grid.countAliveNeighbours(i, j)

			if cellState && aliveNeighboursCount >= 2 && aliveNeighboursCount < 4 {
				nextGenGrid.cells[i][j] = true
			}
			// Any dead cell with exactly three live neighbours becomes a live cell.
			if !cellState && aliveNeighboursCount == 3 {
				nextGenGrid.cells[i][j] = true
			}
		}
	}

	return
}
