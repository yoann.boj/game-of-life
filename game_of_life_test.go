package game_of_life

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestUnderPopulation(t *testing.T) {
	t.Run("cell with less than 2 neighbors die 1x1", func(t *testing.T) {
		grid := NewGridFromCells([][]bool{{true}})

		assert.Equal(t, NewGridFromCells([][]bool{{false}}), ComputeNextGeneration(grid))
	})

	t.Run("cell with less than 2 neighbors die 2x2", func(t *testing.T) {
		grid := NewGridFromCells([][]bool{
			{true, false},
			{false, false},
		})

		expectedGrid := NewGridFromCells([][]bool{
			{false, false},
			{false, false},
		})
		assert.Equal(t, expectedGrid, ComputeNextGeneration(grid))
	})

	t.Run("cell with less than 2 neighbors die 2x2, other configuration 2", func(t *testing.T) {
		grid := NewGridFromCells([][]bool{
			{false, true},
			{false, false},
		})

		expectedGrid := NewGridFromCells([][]bool{
			{false, false},
			{false, false},
		})
		assert.Equal(t, expectedGrid, ComputeNextGeneration(grid))
	})

	t.Run("cell with more than 2 neighbors don't die 2x2,  configuration 3", func(t *testing.T) {
		grid := NewGridFromCells([][]bool{
			{true, true},
			{true, true},
		})

		expectedGrid := NewGridFromCells([][]bool{
			{true, true},
			{true, true},
		})
		assert.Equal(t, expectedGrid, ComputeNextGeneration(grid))
	})

	t.Run("cell with less than 2 neighbors die 3x3", func(t *testing.T) {
		grid := NewGridFromCells([][]bool{
			{true, false, false},
			{false, false, false},
			{false, false, true},
		})

		expectedGrid := NewGridFromCells([][]bool{
			{false, false, false},
			{false, false, false},
			{false, false, false},
		})
		assert.Equal(t, expectedGrid, ComputeNextGeneration(grid))
	})
}

func TestOverPopulation(t *testing.T) {
	t.Run("cell with more than 3 neighbors die 3x3", func(t *testing.T) {
		grid := NewGridFromCells([][]bool{
			{true, true, true},
			{true, true, true},
			{true, true, true},
		})

		expectedGrid := NewGridFromCells([][]bool{
			{true, false, true},
			{false, false, false},
			{true, false, true},
		})
		assert.Equal(t, expectedGrid, ComputeNextGeneration(grid))
	})
}

func TestReborn(t *testing.T) {
	t.Run("dead cell with exactly 3 live neighbors becomes a live cell 2x2", func(t *testing.T) {
		grid := NewGridFromCells([][]bool{
			{true, true},
			{true, false},
		})

		expectedGrid := NewGridFromCells([][]bool{
			{true, true},
			{true, true},
		})
		assert.Equal(t, expectedGrid, ComputeNextGeneration(grid))
	})
	//t.Run("dead cell with exactly 3 live neighbors becomes a live cell 3x3", func(t *testing.T) {
	//	grid := NewGridFromCells([][]bool{
	//		{false, false, false},
	//		{false, true, true},
	//		{false, true, false},
	//	})
	//
	//	expectedGrid := NewGridFromCells([][]bool{
	//		{false, false, false},
	//		{false, true, true},
	//		{false, true, true},
	//	})
	//	assert.Equal(t, expectedGrid, ComputeNextGeneration(grid))
	//})
}
